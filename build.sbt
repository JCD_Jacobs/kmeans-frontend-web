name := "frontend"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  "com.typesafe.akka" %% "akka-remote" % "2.2.1",
  "com.typesafe.akka" %% "akka-cluster" % "2.2.1"
)

play.Project.playScalaSettings

scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature")

scalariformSettings

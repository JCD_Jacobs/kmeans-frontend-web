Requirements
============
* Java Runtime Environment
* Java Development Kit
* Scala Build Tool (sbt)
* Play Framework (play)


Configuration
=============
The configuration is in conf/application.conf
If you want to run the kmeans frontend on multiple computers, make shure akka.remote.netty.tcp.hostname and kmeans.backendActor are correct.


Run
===
run debug mode:

    $play run

run production mode: (Ctrl+D will not kill the server, but Ctrl + c does)

    $play start
    

compile and run:

    $play clean stage
    $./target/universal/stage/bin/frontend

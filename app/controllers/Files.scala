package controllers

import java.io.{ FileNotFoundException, File }
import play.api.mvc._
import play.api.Play
import play.api.Play.current

object Files extends Controller with Secured {
  val baseDir = new File(".").getCanonicalPath.replaceAll("/target/universal/stage", "")
  val datasetDir = baseDir + "/datasets"
  val resultsDir = baseDir + "/results"

  def getFiles(dirPath: String): Array[String] = {
    val dir = new File(dirPath)

    if (dir.exists() && dir.isDirectory()) {
      dir.list().sorted
    } else {
      dir.mkdirs()
      Array[String]()
    }
  }

  def getFileName(fileID: Int, dir: String): String = {
    val files = getFiles(dir)
    if (fileID >= 0 && files.length > fileID) {
      files(fileID)
    } else {
      throw new FileNotFoundException()
    }
  }

  private def getFile(fileID: Int, dir: String): File = {
    new File("%s/%s".format(dir, getFileName(fileID, dir)))
  }

  def files = {
    withAuth { username =>
      implicit request =>
        Ok(views.html.files(getFiles(datasetDir).toList))
    }
  }

  def uploadGet = withAuth { username =>
    implicit request =>
      Ok(views.html.upload())
  }

  def uploadPost = withAuth(parse.multipartFormData) { username =>
    implicit request =>
      request.body.file("file").map { file =>
        val filename = file.filename
        val contentType = file.contentType
        file.ref.moveTo(new File(datasetDir + "/" + filename))

        Redirect(routes.Files.files).flashing("success" -> s"$filename uploaded")
      }.getOrElse {
        Redirect(routes.Files.uploadGet).flashing("error" -> "Missing file")
      }
  }

  private def deleteFile(fileID: Int, dir: String, redirect: Call): EssentialAction = {
    withAuth { username =>
      implicit request => {

        val fileName = getFileName(fileID, dir)
        val file = getFile(fileID, dir)

        if (!file.delete()) {
          Redirect(redirect).flashing("error" -> s"$fileName can not be deleted")
        } else {
          Redirect(redirect).flashing("success" -> s"$fileName deleted")
        }
      }
    }
  }

  def deleteDataset(fileID: Int) = deleteFile(fileID, datasetDir, routes.Files.files)

  def deleteResult(fileID: Int) = deleteFile(fileID, resultsDir, routes.Jobs.jobs)

  private def showFile(fileID: Int, dir: String): EssentialAction = {

    val fileName = getFileName(fileID, dir)

    withAuth { username =>
      implicit request => {
        Ok.sendFile(content = new File(dir + "/" + fileName), inline = true)
      }
    }
  }

  def result(fileID: Int) = showFile(fileID, resultsDir)

  def dataset(fileID: Int) = showFile(fileID, datasetDir)
}

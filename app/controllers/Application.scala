package controllers

import play.api.mvc._
import play.api.libs.concurrent.Akka
import play.api.Play.current
import akka.actor.Props

object Application extends Controller with Secured {

  val frontendActor = Akka.system.actorOf(Props[actors.FrontendActor], name = "frontendActor")

  def index = Action { implicit request =>
    Ok(views.html.index())
  }
}
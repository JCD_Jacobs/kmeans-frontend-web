package controllers

import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import actors.protocols.Frontend._
import actors.protocols.Backend._
import akka.pattern.ask
import scala.concurrent.duration._
import akka.cluster.Member

import controllers.Application.frontendActor

object Status extends Controller with Secured {

  def status() = {
    val statusFuture = frontendActor.ask(GetClusterStatusMessage)(1.second)

    Action.async { implicit request =>
      statusFuture.map {
        case ClusterStatusMessage(members) => {
          Ok(views.html.status(members))
        }
        case error: String => {
          implicit val flash = Flash(Map("error" -> error))
          Ok(views.html.status(List[Member]()))
        }
      }
    }
  }
}
package controllers

import play.api.mvc._
import play.api.data._
import play.api.data.Forms._

import views._

object Auth extends Controller {

  val loginForm = Form(
    tuple(
      "user" -> nonEmptyText,
      "password" -> nonEmptyText
    ) verifying ("Invalid user or password", result => result match {
        case (user, password) => check(user, password)
      })
  )

  def check(username: String, password: String) = {
    (username == "admin" && password == "password")
  }

  def loginGet = Action { implicit request =>
    Ok(html.login(loginForm))
  }

  def loginPost = Action { implicit request =>
    val redirectPath = request.session.get("target").map { path => path }.getOrElse { "/" }

    loginForm.bindFromRequest.fold(
      formWithErrors => BadRequest(html.login(formWithErrors)),
      form => Redirect(redirectPath).withSession(Security.username -> form._1)
    )
  }

  def logout = Action {
    Redirect(routes.Auth.loginGet).withNewSession.flashing(
      "success" -> "You are now logged out."
    )
  }
}

/**
 * Provide security features
 */
trait Secured {

  def username(request: RequestHeader) = request.session.get(Security.username)

  def onUnauthorized(request: RequestHeader) = Results.Redirect(routes.Auth.loginGet).withSession("target" -> request.uri)

  def withAuth[A](p: BodyParser[A] = BodyParsers.parse.anyContent)(f: => String => Request[A] => Result): EssentialAction = {
    Security.Authenticated(username, onUnauthorized) { user =>
      Action(p)(request => f(user)(request))
    }
  }

  def withAuth(f: => String => Request[AnyContent] => Result): EssentialAction = {
    withAuth(BodyParsers.parse.anyContent)(f)
  }
}

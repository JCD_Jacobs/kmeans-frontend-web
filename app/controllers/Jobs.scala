package controllers

import java.util.Calendar
import java.text.SimpleDateFormat
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import akka.pattern.ask
import scala.concurrent.duration._
import actors.protocols.Backend._
import actors.protocols.Frontend._
import java.io.File

import controllers.Application.frontendActor

case class RunParameters(numClusters: Int, threshold: Double, zTransform: Boolean, numNodes: Int)

object Jobs extends Controller with Secured {

  val runForm = Form(
    mapping(
      "numClusters" -> number(min = 1),
      "threshold" -> of[Double].verifying(_ > 0.0).verifying(_ <= 1.0),
      "zTransform" -> boolean,
      "numNodes" -> number(min = 1, max = 8)
    )(RunParameters.apply)(RunParameters.unapply)
  )

  def runFormGet(fileID: Int) = withAuth { username =>
    implicit request => {
      val inputFileName = Files.getFileName(fileID, Files.datasetDir)
      val filledRunForm = runForm.fill(RunParameters(16, 0.00001, true, 1))

      Ok(views.html.run(filledRunForm, inputFileName))
    }
  }

  def runFormPost(fileID: Int) = withAuth { username =>
    implicit request => {
      val inputFileName = Files.getFileName(fileID, Files.datasetDir)
      val time = new SimpleDateFormat("yyyyMMdd_HHmmss_").format(Calendar.getInstance().getTime())
      val outputFileName = time + inputFileName

      runForm.bindFromRequest.fold(
        formWithErrors => BadRequest(views.html.run(formWithErrors, inputFileName)),
        form => {
          val inputFilePath = Files.datasetDir + "/" + inputFileName
          val outputFilePath = Files.resultsDir + "/" + outputFileName
          // send request to kmeans
          frontendActor ! JobMessage(form.numNodes, form.numClusters, form.threshold, form.zTransform, inputFilePath, outputFilePath)

          Redirect(routes.Jobs.jobs).flashing("success" -> "Sent job to backend")
        }
      )
    }
  }

  def jobs() = {
    val jobsFuture = frontendActor.ask(GetRunningJobsMessage)(1.second)
    val files = controllers.Files.getFiles(Files.resultsDir).toList

    Action.async { implicit request =>
      jobsFuture.map {
        case BackendStatusMessage(jobs) => {
          Ok(views.html.jobs(jobs.values.toList, files))
        }
        case error: String => {
          implicit val flash = Flash(Map("error" -> error))
          Ok(views.html.jobs(List[JobMessage](), files))
        }
      }
    }
  }
}

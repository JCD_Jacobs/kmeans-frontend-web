package io

import java.io.FileWriter
import java.io.File
import actors.protocols.Backend._

object OutputWriter {

  def write(content: String, to: String) = {
    val writer = new FileWriter(new File(to))
    writer.write(content)
    writer.flush()
    writer.close()
  }

  def getClusterCenters(clusterCenters: Seq[Seq[Double]]): String = {
    val output = new StringBuilder
    clusterCenters.foreach(ob => {
      ob.foreach(attr => output ++= " " + attr)
      output ++= "\n"
    })

    output.toString()
  }

  def getParameters(jobMessage: JobMessage): String = {
    "\nParameters:" +
      "\nnumNodes %d".format(jobMessage.numNodes) +
      "\nnumClusters %d".format(jobMessage.numClusters) +
      "\nthreshold %f".format(jobMessage.threshold) +
      "\nzScoreTransform %s".format(jobMessage.zScoreTransform.toString) +
      "\ninputFilePath %s".format(jobMessage.inputFilePath) + "\n"
  }

  def getStats(result: ResultMessage): String = {
    "\nStats:" +
      "\ntime read and transform: %d ms".format(result.timeReadTransform) +
      "\ntime to last transmit: %d ms".format(result.timeTransmit) +
      "\ntime to end of compute: %d ms".format(result.timeCompute) +
      "\noptimization passes: %d".format(result.optimizationPasses) +
      "\ndelta sum: %f".format(result.deltaSum) + "\n"
  }

}

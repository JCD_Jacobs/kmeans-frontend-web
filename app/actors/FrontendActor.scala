package actors

import io.OutputWriter
import akka.actor.{ ActorRef, Actor, Terminated, ActorNotFound }
import akka.util.Timeout
import play.api.libs.concurrent.Akka
import play.api.Play.current
import scala.concurrent.Await
import scala.concurrent.duration._
import com.typesafe.config._
import java.util.concurrent.TimeoutException
import protocols.Backend._
import protocols.Frontend._
import akka.cluster.Member

// bridge actor to send messages to remote actor
class FrontendActor() extends Actor {

  val config = ConfigFactory.load()
  val kMeansBackendPath = config.getString("kmeans.backendActor")
  var kMeansBackend: ActorRef = Akka.system.deadLetters

  var jobs = Map[Int, JobMessage]()
  var clusterMembers = List[Member]()
  var kMeansIsAlive = false

  def connectKMeans() = {
    implicit val timeout = Timeout(1.second)
    try {
      kMeansBackend = Await.result(Akka.system.actorSelection(kMeansBackendPath).resolveOne, atMost = 1.second)
      context.watch(kMeansBackend)
      kMeansBackend ! RegisterAsObserverMessage
    } catch {
      case e: ActorNotFound => {}
      case e: TimeoutException => {}
    }
  }

  override def preStart() {
    connectKMeans()
  }

  def receive = {
    case GetRunningJobsMessage => {
      // error detection
      if (kMeansIsAlive) {
        sender ! BackendStatusMessage(jobs)
      } else {
        sender ! "No connection to " + kMeansBackendPath
        connectKMeans()
      }
    }

    case jobMessage: JobMessage => {
      kMeansBackend ! jobMessage
    }

    case result: ResultMessage => {
      OutputWriter.write(
        OutputWriter.getClusterCenters(result.clusterCenters) + OutputWriter.getParameters(jobs(result.jobID)) + OutputWriter.getStats(result),
        to = jobs(result.jobID).outputFilePath
      )
    }

    case BackendStatusMessage(jobs) => {
      kMeansIsAlive = true
      this.jobs = jobs
    }

    case GetClusterStatusMessage => {
      // error detection
      if (kMeansIsAlive) {
        sender ! ClusterStatusMessage(clusterMembers)
      } else {
        sender ! "No connection to " + kMeansBackendPath
        connectKMeans()
      }
    }

    case ClusterStatusMessage(members) => {
      clusterMembers = members
    }

    case terminated: Terminated => {
      kMeansIsAlive = false
    }
  }
}
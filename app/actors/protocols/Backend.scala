package actors.protocols

import akka.actor.Address
import akka.cluster.Member

object Backend {

  // Messages to Backend
  case class JobMessage(numNodes: Int, numClusters: Int, threshold: Double, zScoreTransform: Boolean, inputFilePath: String, outputFilePath: String)

  case class ResultMessage(jobID: Int, clusterCenters: Seq[Seq[Double]], optimizationPasses: Int, deltaSum: Double, timeReadTransform: Long, timeTransmit: Long, timeCompute: Long)

  case object RegisterAsObserverMessage

  // Messages from Backend
  case class BackendStatusMessage(jobs: Map[Int, JobMessage])

  case class ClusterStatusMessage(clusterMembers: List[Member])
}

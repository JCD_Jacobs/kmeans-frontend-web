package actors.protocols

object Frontend {
  case object GetRunningJobsMessage
  case object GetClusterStatusMessage
}
